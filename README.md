# Ansible role for Postgrey installation

## Introduction

[Postgrey](http://postgrey.schweikert.ch/) is a Postfix policy server
implementing greylisting.

This role installs and configure the server.

You can specify whitelisted clients.

## Variables

- **whitelist_clients**: site-specific whitelisted clients
    a list of sites using lots of outgoing servers chosen randomly,
    thus leading to very delayed of even lost mails, is defined in the
    `whitelist_clients_base` variable; it is not recommended to override
    this variable.

## TODO

More daemon options could be interesting (/etc/sysconfig/postgrey).

