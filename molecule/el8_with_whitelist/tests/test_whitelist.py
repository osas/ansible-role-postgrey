import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_whitelist_content(host):
    assert "example.com" in host.file("/etc/postfix/postgrey_whitelist_clients.local").content_string.split()

